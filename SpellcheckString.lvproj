﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="18008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Spell Checker" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="Benchmark" Type="Folder">
				<Property Name="NI.SortType" Type="Int">0</Property>
				<Item Name="Determine Suggestions" Type="Folder">
					<Item Name="Naive Approach" Type="Folder">
						<Item Name="Calculate Distance Full.vi" Type="VI" URL="../Benchmark/Naive Approach/Calculate Distance Full.vi"/>
						<Item Name="Calculate Distance.vi" Type="VI" URL="../Benchmark/Naive Approach/Calculate Distance.vi"/>
						<Item Name="Get Character.vi" Type="VI" URL="../Benchmark/Naive Approach/Get Character.vi"/>
						<Item Name="Naive Approach.vi" Type="VI" URL="../Benchmark/Naive Approach/Naive Approach.vi"/>
						<Item Name="Transposition Cost.vi" Type="VI" URL="../Benchmark/Naive Approach/Transposition Cost.vi"/>
					</Item>
					<Item Name="Peter Norvig Approach" Type="Folder">
						<Item Name="Deletion" Type="Folder">
							<Item Name="Benchmark Deletion Options.vi" Type="VI" URL="../Benchmark/Deletion/Benchmark Deletion Options.vi"/>
							<Item Name="Deletion Delete From Array.vi" Type="VI" URL="../Benchmark/Deletion/Deletion Delete From Array.vi"/>
							<Item Name="Deletion Filter Array.vi" Type="VI" URL="../Benchmark/Deletion/Deletion Filter Array.vi"/>
							<Item Name="Deletion In Place.vi" Type="VI" URL="../Benchmark/Deletion/Deletion In Place.vi"/>
							<Item Name="Deletion Replace Substring.vi" Type="VI" URL="../Benchmark/Deletion/Deletion Replace Substring.vi"/>
							<Item Name="Deletion String Subset.vi" Type="VI" URL="../Benchmark/Deletion/Deletion String Subset.vi"/>
						</Item>
						<Item Name="Insertion" Type="Folder">
							<Property Name="NI.SortType" Type="Int">0</Property>
							<Item Name="Benchmark Insertion Options.vi" Type="VI" URL="../Benchmark/Insertion/Benchmark Insertion Options.vi"/>
							<Item Name="Insertion Insert Into Array.vi" Type="VI" URL="../Benchmark/Insertion/Insertion Insert Into Array.vi"/>
							<Item Name="Insertion Matrix.vi" Type="VI" URL="../Benchmark/Insertion/Insertion Matrix.vi"/>
							<Item Name="Insertion Replace Substring.vi" Type="VI" URL="../Benchmark/Insertion/Insertion Replace Substring.vi"/>
							<Item Name="Insertion Rotate Array.vi" Type="VI" URL="../Benchmark/Insertion/Insertion Rotate Array.vi"/>
							<Item Name="Insertion String Subset.vi" Type="VI" URL="../Benchmark/Insertion/Insertion String Subset.vi"/>
						</Item>
						<Item Name="Substitution" Type="Folder">
							<Item Name="Benchmark Substitution Options.vi" Type="VI" URL="../Benchmark/Substituition/Benchmark Substitution Options.vi"/>
							<Item Name="Substitution In Place.vi" Type="VI" URL="../Benchmark/Substituition/Substitution In Place.vi"/>
							<Item Name="Substitution Matrix.vi" Type="VI" URL="../Benchmark/Substituition/Substitution Matrix.vi"/>
							<Item Name="Substitution Replace Array Subset.vi" Type="VI" URL="../Benchmark/Substituition/Substitution Replace Array Subset.vi"/>
							<Item Name="Substitution Replace Substring.vi" Type="VI" URL="../Benchmark/Substituition/Substitution Replace Substring.vi"/>
							<Item Name="Substitution String Subset.vi" Type="VI" URL="../Benchmark/Substituition/Substitution String Subset.vi"/>
						</Item>
						<Item Name="Transposition" Type="Folder">
							<Item Name="Benchmark Transposition Options.vi" Type="VI" URL="../Benchmark/Transposition/Benchmark Transposition Options.vi"/>
							<Item Name="Transposition In Place.vi" Type="VI" URL="../Benchmark/Transposition/Transposition In Place.vi"/>
							<Item Name="Transposition Replace Substring.vi" Type="VI" URL="../Benchmark/Transposition/Transposition Replace Substring.vi"/>
							<Item Name="Transposition String Subset.vi" Type="VI" URL="../Benchmark/Transposition/Transposition String Subset.vi"/>
						</Item>
						<Item Name="Peter Norvig Approach.vi" Type="VI" URL="../Benchmark/Peter Norvig Approach/Peter Norvig Approach.vi"/>
					</Item>
					<Item Name="SDSC Approach" Type="Folder">
						<Item Name="Calculate Input Entry Deletion.vi" Type="VI" URL="../Benchmark/SDSC Approach/Calculate Input Entry Deletion.vi"/>
						<Item Name="Perform Comparisons.vi" Type="VI" URL="../Benchmark/SDSC Approach/Perform Comparisons.vi"/>
						<Item Name="Precalculation.vi" Type="VI" URL="../Benchmark/SDSC Approach/Precalculation.vi"/>
					</Item>
					<Item Name="Benchmark Determine Suggestion Correctness.vi" Type="VI" URL="../Benchmark/Determine Suggestions/Benchmark Determine Suggestion Correctness.vi"/>
					<Item Name="Benchmark Determine Suggestions Actual Implementation.vi" Type="VI" URL="../Benchmark/Determine Suggestions/Benchmark Determine Suggestions Actual Implementation.vi"/>
					<Item Name="Benchmark Determine Suggestions.vi" Type="VI" URL="../Benchmark/Determine Suggestions/Benchmark Determine Suggestions.vi"/>
				</Item>
				<Item Name="Remove Puctuation" Type="Folder">
					<Item Name="Benchmark Remove Punctuation Options.vi" Type="VI" URL="../Benchmark/Remove Punctuation/Benchmark Remove Punctuation Options.vi"/>
					<Item Name="Remove Punctuation Equate Array.vi" Type="VI" URL="../Benchmark/Remove Punctuation/Remove Punctuation Equate Array.vi"/>
					<Item Name="Remove Punctuation Index Array.vi" Type="VI" URL="../Benchmark/Remove Punctuation/Remove Punctuation Index Array.vi"/>
					<Item Name="Remove Punctuation Search 1D Array.vi" Type="VI" URL="../Benchmark/Remove Punctuation/Remove Punctuation Search 1D Array.vi"/>
					<Item Name="Remove Punctuation Search and Replace.vi" Type="VI" URL="../Benchmark/Remove Punctuation/Remove Punctuation Search and Replace.vi"/>
				</Item>
				<Item Name="Search" Type="Folder">
					<Item Name="Benchmark Search Options.vi" Type="VI" URL="../Benchmark/Search/Benchmark Search Options.vi"/>
					<Item Name="Binary Search.vi" Type="VI" URL="../Benchmark/Search/Binary Search.vi"/>
					<Item Name="Variant Lookup Table  - Load Dictionary.vi" Type="VI" URL="../Benchmark/Search/Variant Lookup Table  - Load Dictionary.vi"/>
					<Item Name="Variant Lookup Table - Dictionary Loaded.vi" Type="VI" URL="../Benchmark/Search/Variant Lookup Table - Dictionary Loaded.vi"/>
					<Item Name="Variant Lookup Table.vi" Type="VI" URL="../Benchmark/Search/Variant Lookup Table.vi"/>
				</Item>
				<Item Name="Split String" Type="Folder">
					<Item Name="Benchmark Split String Options.vi" Type="VI" URL="../Benchmark/Split String/Benchmark Split String Options.vi"/>
					<Item Name="Split String Manual Space Search.vi" Type="VI" URL="../Benchmark/Split String/Split String Manual Space Search.vi"/>
					<Item Name="Split String Scan for Tokens.vi" Type="VI" URL="../Benchmark/Split String/Split String Scan for Tokens.vi"/>
					<Item Name="Split String Spreadsheet String to Array.vi" Type="VI" URL="../Benchmark/Split String/Split String Spreadsheet String to Array.vi"/>
				</Item>
				<Item Name="Read Common Misspellings List.vi" Type="VI" URL="../Benchmark/Read Common Misspellings List.vi"/>
				<Item Name="Read Dictionary.vi" Type="VI" URL="../Benchmark/Read Dictionary.vi"/>
				<Item Name="Read Example Text.vi" Type="VI" URL="../Benchmark/Read Example Text.vi"/>
				<Item Name="Remove Duplicates from 1D Array.vi" Type="VI" URL="../Benchmark/Remove Duplicates from 1D Array.vi"/>
				<Item Name="Running Average.vi" Type="VI" URL="../Benchmark/Running Average.vi"/>
			</Item>
			<Item Name="Spell Checker.lvlib" Type="Library" URL="../SpellcheckEngine/Spell Checker.lvlib"/>
		</Item>
		<Item Name="Example Text.vi" Type="VI" URL="../Example Text.vi"/>
		<Item Name="SpellcheckString.lvclass" Type="LVClass" URL="../SpellcheckString.lvclass"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="MGI Create Directory Chain Behavior Enum.ctl" Type="VI" URL="/&lt;userlib&gt;/_MGI/File/MGI Create Directory Chain Behavior Enum.ctl"/>
				<Item Name="MGI Create Directory Chain.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/File/MGI Create Directory Chain.vi"/>
				<Item Name="Random Number - Within Range__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/numeric/numeric.llb/Random Number - Within Range__ogtk.vi"/>
				<Item Name="Random Number Within Range - DBL__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/numeric/numeric.llb/Random Number Within Range - DBL__ogtk.vi"/>
				<Item Name="Random Number Within Range - I32__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/numeric/numeric.llb/Random Number Within Range - I32__ogtk.vi"/>
				<Item Name="Random Number Within Range__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/numeric/numeric.llb/Random Number Within Range__ogtk.vi"/>
				<Item Name="To Proper Case (String Array)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/To Proper Case (String Array)__ogtk.vi"/>
				<Item Name="To Proper Case (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/To Proper Case (String)__ogtk.vi"/>
				<Item Name="To Proper Case__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/To Proper Case__ogtk.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Close File+.vi"/>
				<Item Name="compatReadText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatReadText.vi"/>
				<Item Name="Control.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/QSI/QControls/QControl Classes/Control/Control.lvclass"/>
				<Item Name="Dflt Data Dir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Dflt Data Dir.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="Generic.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/QSI/QControls/QControl Classes/Generic/Generic.lvclass"/>
				<Item Name="GObject.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/QSI/QControls/QControl Classes/GObject/GObject.lvclass"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVDataSocketStatusTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVDataSocketStatusTypeDef.ctl"/>
				<Item Name="LVKeyNavTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVKeyNavTypeDef.ctl"/>
				<Item Name="LVPoint32TypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPoint32TypeDef.ctl"/>
				<Item Name="LVPositionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPositionTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_Matrix.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/Matrix/NI_Matrix.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Open File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Open File+.vi"/>
				<Item Name="Read Delimited Spreadsheet (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet (DBL).vi"/>
				<Item Name="Read Delimited Spreadsheet (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet (I64).vi"/>
				<Item Name="Read Delimited Spreadsheet (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet (string).vi"/>
				<Item Name="Read Delimited Spreadsheet.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet.vi"/>
				<Item Name="Read File+ (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read File+ (string).vi"/>
				<Item Name="Read Lines From File (with error IO).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Lines From File (with error IO).vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="String.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/QSI/QControls/QControl Classes/String/String.lvclass"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Write Delimited Spreadsheet (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (DBL).vi"/>
				<Item Name="Write Delimited Spreadsheet (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (I64).vi"/>
				<Item Name="Write Delimited Spreadsheet (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (string).vi"/>
				<Item Name="Write Delimited Spreadsheet.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet.vi"/>
				<Item Name="Write Spreadsheet String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Spreadsheet String.vi"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
